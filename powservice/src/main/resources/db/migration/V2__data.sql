INSERT INTO plan_of_work VALUES (
	1, 
	'Alabama Agricultural and Mechanical University (AAMU)',
	
	'AAMU is an 1890 land-grant institution with a comprehensive university Carnegie classification, 
	functioning in the areas of teaching, research, and extension. AAMU is a doctoral degree granting 
	institution with strong graduate programs in the science, technology, engineering, and mathematics 
	(STEM) disciplines.',
	
	'The Plan of Work Merit Review is an inclusive multi-phase process with Extension and Research at all three 
	land-grant universities in Alabama. Phase I includes Extension and Research teams identifying program and 
	research needs shared by county stakeholders and advisory groups.', 
	
	'Submission in process',
	
	'EXTENSION:\n
	The Alabama Cooperative Extension System and the Tuskegee University Cooperative Extension Program (ACES/TUCEP) 
	utilize a comprehensive grasstops and grassroots needs assessment process. State-level constituent or consensus 
	building groups, non-governmental agencies, community-based organizations, and governmental agencies are 
	encouraged to participate in grasstops needs assessment activities by inviting both traditional and 
	non-traditional stakeholder groups.',
	
	'EXTENSION:\n
	A comprehensive approach to needs identification is utilized given the complexity and scope of issues facing the 
	citizens of Alabama. For ACES/TUCEP, the comprehensive needs assessment begins with the engagement of key external
	grasstops stakeholders to determine priority needs affecting Alabamians.',
	
	'EXTENSION:\n
	Strategic program initiatives are identified from the comprehensive grasstops and grassroots needs assessment 
	activities. Program leaders collaborate on the development of a logic model for each strategic program initiative 
	focusing on specific objectives, outputs, and outcomes that allow for application across various program areas. 
	Each logic model includes an evaluation plan.',
	
	'EXTENSION:\n
	ACES/TUCEP program leaders lead respective program teams, consisting of Extension Specialists, Agents, 
	Resource Specialists, and Farm Management Specialists to identify state-level constituent or consensus 
	building groups, non-governmental agencies, community-based organizations, and governmental agencies. 
	Methods for identifying these groups included existing advisory committees and interagency directories.',
	
	2020);
	
INSERT INTO plan_of_work VALUES (2, 'Auburn University (AU)', 'Sample executive summary 2',
	'Sample merit peer review 2', 'Submission in process 2', 'Stakeholder actions 2',
	'Stakeholder collection methods 2', 'Stakeholder how considered 2', 'Stakeholder id method 2', 2022);
	
INSERT INTO critical_issue VALUES (1, 5, 'Family Home', 'INTERMEDIATE', 2);
INSERT INTO critical_issue VALUES (2, 4, 'Sustainable Energy', 'LONG', 1);