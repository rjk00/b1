import React, { Component } from 'react';
import axios from 'axios';
import PowTable from './PowTable'; 
import Container from 'react-bootstrap/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import config from '../../app.config';

export default class PowHome extends Component {
    constructor(props){
        super(props);
        this.state = {
            plans: []
        }
    }
    
    componentDidMount(){
        this.getPlans()
    }
    
    async getPlans() {
	//console.log("API URL......." + window.$API_URL);
        const response = await axios.get(window.$API_URL + '/plans');
        this.setState({
            plans: response.data
        })
    }
   
    render(){
        return(
            <Container>
                <Jumbotron>
                    <h1 className="text-center">Plan Of Working aaa</h1>
                </Jumbotron>
                <h3>University of NIFA Combined Research and Extension</h3>
                <PowTable plans={this.state.plans}/>
           </Container> 
        );
    }
}
